# -*- coding: utf-8 -*-
import pygetwindow as gw
import pyautogui
import win32api, win32con
import time
import datetime


Click_interval = 0.03

_quit = False


def mouse_click(times=1, interval=0.1):
    position = pyautogui.position()
    for i in range(int(times)):
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
        time.sleep(interval / 2)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
        time.sleep(interval / 2)

        if pyautogui.position() != position:
            global _quit
            _quit = True
            return


def mouse_click_seconds(seconds, interval=Click_interval):
    mouse_click(int(seconds / interval), interval)


def detect(image_file):
    return pyautogui.locateOnScreen(image_file, grayscale=True, confidence=0.9)


def detect_and_move(image_file):
    result = detect(image_file)
    if result:
        pyautogui.moveTo(pyautogui.center(result))
        return True

    return False


def control_mouse():
    now = datetime.datetime.now()

    # ゲーム起動しているかを確認する。
    windows = gw.getWindowsWithTitle('Sakura Clicker')
    if len(windows) == 0:
        print('not found "Sakura Clicker" application.')
        return False

    window = windows[0]

    # ゲーム画面を前面に出す。
    window.activate()

    # ゲーム画面の位置を取得する。
    region = (window.topleft.x, window.topleft.y, window.size.width, window.size.height)
    center_x = int(region[0] + (region[2] / 2))
    center_y = int(region[1] + (region[3] / 2))

    # ゲーム画面をキャプチャする。
    #capture = pyautogui.screenshot()

    # Gold Fountain
    if detect_and_move('gold_fountain_button.png'):
        print('gold fountain')
        mouse_click()
        pyautogui.move(0, -200)
        mouse_click_seconds(20)

    # Yellow Pet
    if detect_and_move('yellow_pet.png'):
        print('yellow pet')
        mouse_click()

    # Pink Pet
    if detect_and_move('pink_pet.png'):
        print('pink pet')
        mouse_click_seconds(10)

    # Chest
    if detect_and_move('chest.png'):
        print('chest')
        mouse_click(12)

    # Leave Boss
    if detect_and_move('leave_boss_button.png'):
        print('leave boss')
        pyautogui.moveTo(center_x, center_y)
        mouse_click_seconds(5)

    enter_boss = detect('enter_boss_button.png')

    # 下にスクロールして、下２つのボタンからのみ購入する。
    pyautogui.scroll(-30)
    if now.second >= 58:
        print('buy')
        positions = []
        for position in pyautogui.locateAllOnScreen('buy_button.png'):
            positions.append(position)

        for position in reversed(positions):
            # Enter Boss のときには一番下のボタンからのみ購入する。
            y = position[1]
            if (enter_boss and y > center_y + (region[3] / 4)) or (not enter_boss and y > center_y):
                pyautogui.moveTo(position)
                mouse_click()

    # Enter Boss
    if now.minute % 5 == 0 and now.second >= 58:
        if enter_boss:
            pyautogui.moveTo(pyautogui.center(enter_boss))
            mouse_click()

    pyautogui.moveTo(center_x, center_y)
    mouse_click_seconds(0.1)

    return True


def main():
    while not _quit:
        if not control_mouse():
            break

    print('quit')


if __name__ == '__main__':
    main()
