import pygetwindow as gw
import pyautogui
import win32api, win32con
import time


Click_interval = 0.03

_quit = False


def mouse_click(times=1, interval=0.1):
    position = pyautogui.position()
    for i in range(int(times)):
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTDOWN, 0, 0)
        time.sleep(interval / 2)
        win32api.mouse_event(win32con.MOUSEEVENTF_LEFTUP, 0, 0)
        time.sleep(interval / 2)

        if pyautogui.position() != position:
            global _quit
            _quit = True
            return


def mouse_click_seconds(seconds, interval=Click_interval):
    mouse_click(int(seconds / interval), interval)


def detect(image_file):
    return pyautogui.locateOnScreen(image_file, grayscale=True, confidence=0.9)


def detect_and_move(image_file):
    result = detect(image_file)
    if result:
        pyautogui.moveTo(pyautogui.center(result))
        return True

    return False


def control_mouse():
    # ゲーム起動しているかを確認する。
    windows = gw.getWindowsWithTitle('Sakura Clicker')
    if len(windows) == 0:
        print('not found "Sakura Clicker" application.')
        return False

    window = windows[0]

    # ゲーム画面を前面に出す。
    window.activate()

    # ゲーム画面の位置を取得する。
    region = (window.topleft.x, window.topleft.y, window.size.width, window.size.height)
    center_x = int(region[0] + (region[2] / 2))
    center_y = int(region[1] + (region[3] / 2))

    # level up
    for position in pyautogui.locateAllOnScreen('buy_button.png'):
        pyautogui.moveTo(position)
        mouse_click_seconds(1)

    pyautogui.moveTo(center_x, center_y)
    mouse_click_seconds(1)

    return True


def main():
    while not _quit:
        if not control_mouse():
            break

    print('quit')


if __name__ == '__main__':
    main()
